from daiquiri.core.components import ComponentActor


class MonitorActor(ComponentActor):
    def method(self):
        """This actor determines whether the queue is ready to process

        Returns:
            ready (boolean): If the queue is ready to process
        """
        return True
