CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

conda run -n tomodemo-tools-env supervisorctl stop all



# Hardcore kill

pkill -f supervisord

pkill -9 -f redis-server
pkill -9 -f beacon-server
pkill -9 -f tomovis
killall tailon
killall pnpm
killall npm
pkill -9 -f "daiquiri-ui.git/node_modules"  # node

# anything launched from this envs
pkill -9 -f tomodemo-bliss-env
pkill -9 -f tomodemo-daiquiri-env
pkill -9 -f tomodemo-tools-env
pkill -9 -f tomodemo-ui-env


# Stop the dockers

if groups | grep -q docker; then
    DOCKER_CMD_PREFIX=""
else
    DOCKER_CMD_PREFIX="sudo "
fi
${DOCKER_CMD_PREFIX}docker kill tomodemo-mariadb
${DOCKER_CMD_PREFIX}docker kill tomodemo-activemq
${DOCKER_CMD_PREFIX}docker kill tomodemo-zocalo
${DOCKER_CMD_PREFIX}docker kill tomodemo-rabbitmq
${DOCKER_CMD_PREFIX}docker kill tomodemo-sidecar
