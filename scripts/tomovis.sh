#!/bin/bash

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh


# To have a separator inside supervisor logs
T=$(date +%R)
echo "$T mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"

conda activate tomodemo-tomovis-env

exec tomovis -v --config beacon:///services/tomovis.yml
