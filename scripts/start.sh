CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

#
# Start or restart supervisord
#

conda deactivate  # make sure no conda env are activated

conda run -n tomodemo-tools-env supervisorctl pid
if [ $? -eq 0 ]; then
   conda run -n tomodemo-tools-env supervisorctl reload
else
   echo "Start supervisord"
   conda run -n tomodemo-tools-env supervisord -c daemon/config/supervisor.conf
fi


echo "Available services"
echo "- Landing page:      http://localhost:9010"
echo "- Multivisor:        http://localhost:22000"
echo "- Supervisor:        http://localhost:9001"
echo "- Ewoks monitor:     http://localhost:5555   (celery flower)"
