#!/bin/bash

print_nb_commits () {
  # Display a # for each extra commits on top of the main origin branch
  pushd $1 > /dev/null
  local nbprev=$(git rev-list $2...HEAD --left-right | grep '<' | wc -l)
  local nb=$(git rev-list $2...HEAD --left-right | grep '>' | wc -l)
  local nbprev=$((nbprev+1))
  local nb=$((nb+1))
  local stage=$(git status -s | wc -l)
  local bar=$(seq -s+ $nb | tr -d '[:digit:]')
  local barprev=$(seq -s+ $nbprev | tr -d '[:digit:]')
  printf '  %18s: %s\e[1;32m%s\e[m...\e[1;32m%s\e[m' $1 $2 $barprev $bar
  if [ $stage -ne 0 ]; then
    printf '\e[1;31m+\e[m'
  fi
  printf 'HEAD'
  printf '\n'
  popd > /dev/null
}

git submodule foreach git fetch origin

print_nb_commits daiquiri.git origin/main
print_nb_commits daiquiri-ui.git origin/main
print_nb_commits tomovis.git origin/main
print_nb_commits sidecar.git origin/main
print_nb_commits bliss.git origin/2.1.x
print_nb_commits bliss-tomo.git origin/main
print_nb_commits fscan.git origin/master
