set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

# Try to setup working envs

# Generate env in order to freeze them
#
# conda env export > my_environment.yml

set +x
echo "========================================="
echo "              BLISS ENV"
echo "========================================="
set -x

conda env remove -n $1

cd bliss.git
make bl_env NAME=tomodemo-bliss-env
make demo_env NAME=tomodemo-bliss-env
cd ..
conda activate tomodemo-bliss-env
# pip install -e bliss.git
pip install -e bliss.git/blisswriter
pip install -e fscan.git
pip install -e bliss-tomo.git --no-deps
pip install --no-deps -e id00.git

conda deactivate

set +x
echo "========================================="
echo "                   DONE"
echo "========================================="
set -x
