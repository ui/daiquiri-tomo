#! /bin/bash
# Wrapper to run and stop properly a docker container with Supervisor
#
# The first argument have to be the name of the container, other parameters
# are passed to a "sudo docker run" command.
#
# "sudo docker stop CONTAINER_NAME" is called with a SIGUSR1 is received.
#
# The supervisor program have to be setup with "stopsignal=USR1"

# Separator inside supervisor logs
T=$(date +%R)
echo "$T mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"

NAME=$1
ARGS=${@:2}
PID=$$

echo "PID: ${PID}"

if groups | grep -q docker; then
    DOCKER_CMD_PREFIX=""
else
    DOCKER_CMD_PREFIX="sudo "
fi

shutdown()
{
    CMD="${DOCKER_CMD_PREFIX}docker stop ${NAME}"
    echo "EXEC: ${CMD}"
    ${CMD}
    exit 0
}

echo 'wrapper' > /proc/$$/comm

trap 'shutdown' SIGUSR1

CMD="${DOCKER_CMD_PREFIX}docker run --name ${NAME} ${ARGS}"
echo "EXEC: ${CMD}"
${CMD} &  # The & is mandatory to be able to catch the signal
wait
