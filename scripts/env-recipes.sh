set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

recreate_and_activate_env() {
  conda env remove -n $1
  mamba create --yes -n $1
  conda activate $1
  conda config --env --set channel_priority false
  conda config --env --add channels conda-forge
  conda config --env --remove channels defaults
  conda config --env --append channels esrf-bcu
  conda config --env --append channels tango-controls
}

# Try to setup working envs

# Generate env in order to freeze them
#
# conda env export > my_environment.yml

set +x
echo "========================================="
echo "              DAIQUIRI VIDEO STREAM"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-daiquiri-stream-env
mamba install --yes --file https://gitlab.esrf.fr/ui/video-streamer-mpeg/-/raw/master/requirements-conda.txt
pip install git+https://gitlab.esrf.fr/ui/video-streamer-mpeg

conda deactivate

set +x
echo "========================================="
echo "                 UI ENV"
echo "========================================="
set -x

mamba create --yes -n tomodemo-ui-env
conda activate tomodemo-ui-env

mamba install --yes "nodejs==16.*"
npm install -g pnpm

pushd daiquiri-ui.git
pnpm install
popd

conda deactivate

set +x
echo "========================================="
echo "              LIMA ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-lima-env
mamba install --yes --file scripts/requirements/lima.txt

pip install -e bliss-tomo.git/ --no-deps
pip install -e bliss.git/ --no-deps

conda deactivate

set +x
echo "========================================="
echo "              TOOLS ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-tools-env
mamba install --yes "python==3.7.*"
mamba install --yes werkzeug==2.0.1 # for multivisor to work
conda deactivate  # make sure we use the right pip
conda activate tomodemo-tools-env
pip install multivisor[web] supervisor pyvirtualdisplay

conda deactivate

set +x
echo "========================================="
echo "              TOMOVIS ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-tomovis-env

# sidecar expect python 3.9 for typing
# tornado 6.1 is mandatory to match the one known by pip
# h5grove==1.0.0 else tomovis breaks

mamba install --yes python==3.9 pyopencl ocl-icd-system celery flower graypy junit-xml mysql-connector-python==8.0.29 pydantic python-dotenv pyyaml redis ruamel.yaml silx sqlalchemy tornado=6.1
pip install ewoksjob[worker,beacon,redis,monitor] -r sidecar.git/requirements-pip.txt h5grove==1.0.0
pip install -e sidecar.git/ --no-deps
pip install -e bliss-tomo.git/ --no-deps
pip install -e tomovis.git/

conda deactivate
