CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

ENVFILE=tomovis.git/deploy/conda-prod.yml
ENV=tomovis-deploy
conda create -q --yes -n $ENV
conda activate $ENV
mamba install -y --file sidecar.git/requirements.txt "silx >= 1.1"
pip install -r sidecar.git/requirements-pip.txt -r tomovis.git/requirements.txt
conda env export --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE
sed -i '/.*- tomo==.*/d' $ENVFILE
sed -i '/.*- tomovis==.*/d' $ENVFILE
sed -i '/.*- sidecar==.*/d' $ENVFILE
conda deactivate
conda env remove -n $ENV
