#
# Reread the supervisor configuration
# without restarting all managed applications
#

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh


conda activate tomodemo-tools-env

supervisorctl reread
supervisorctl reload
