export TANGO_HOST=localhost:10000
Export BEACON_HOST=localhost:10001
export ISPYB_DATABASE_URL=localhost:4406/test
export ISPYB_DATABASE_USER=test
export ISPYB_DATABASE_PASSWORD=test
export CELERY_BROKER_URL=redis://localhost:10002/2
# export CELERY_BROKER_URL=amqp://guest:guest@localhost/default
# export CELERY_BROKER_API_URL=http://guest:guest@localhost:15672/api/
export CELERY_BROKER_QUEUE=default
export CELERY_BACKEND_URL=redis://localhost:10002/3
export CELERY_EWOKS_TASK_QUEUES=tomo-sinogram-reconstruction
export EWOKS_GRAPH_LOCATION=pyrsrc://tomovis.resources/graphs
export MIMAS_SPECIFICATIONS=sidecar.celery.mimas.specifications

PWD=$(pwd)

CMD="./scripts/conda-run.sh tomodemo-tomovis-env sidecar.worker ewoks -q ewoks.tomo-sinogram-reconstruction"
echo ${CMD}
${CMD}