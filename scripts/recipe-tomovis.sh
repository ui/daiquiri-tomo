set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

recreate_and_activate_env() {
  conda env remove -n $1
  mamba create --yes -n $1
  conda activate $1
  conda config --env --set channel_priority false
  conda config --env --add channels conda-forge
  conda config --env --remove channels defaults
  # conda config --env --append channels esrf-bcu
  # conda config --env --append channels tango-controls
}

# Try to setup working envs

# Generate env in order to freeze them
#
# conda env export > my_environment.yml

set +x
echo "========================================="
echo "              TOMOVIS ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-tomovis-env

# sidecar expect python 3.9 for typing
# tornado 6.1 is mandatory to match the one known by pip
# h5grove==1.0.0 else tomovis breaks

mamba install --yes python==3.9 pyopencl ocl-icd-system celery flower graypy junit-xml mysql-connector-python==8.0.29 pydantic python-dotenv pyyaml redis-server ruamel.yaml silx sqlalchemy tornado=6.1
pip install ewoksjob[worker,beacon,redis,monitor] -r sidecar.git/requirements-pip.txt h5grove==1.0.0
pip install -e sidecar.git/ --no-deps
pip install -e bliss-tomo.git/ --no-deps
pip install -e tomovis.git/

conda deactivate

set +x
echo "========================================="
echo "                   DONE"
echo "========================================="
set -x
