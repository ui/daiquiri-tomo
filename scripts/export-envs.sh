


ENVFILE=scripts/requirements/tools.yml
ENV=tomodemo-tools-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE

########################################

ENVFILE=scripts/requirements/lima.yml
ENV=tomodemo-lima-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE
sed -i '/.*- bliss-tomo==.*/d' $ENVFILE
sed -i '/.*- bliss==.*/d' $ENVFILE

########################################

ENVFILE=scripts/requirements/ui.yml
ENV=tomodemo-ui-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE

########################################

ENVFILE=scripts/requirements/daiquiri.yml
ENV=tomodemo-daiquiri-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE
sed -i '/.*- nxtomomill==.*/d' $ENVFILE
sed -i '/.*- bliss-tomo==.*/d' $ENVFILE
sed -i '/.*- bliss==.*/d' $ENVFILE
sed -i '/.*- blisswriter==.*/d' $ENVFILE
# sed -i '/.*- tomoscan==.*/d' $ENVFILE
sed -i '/.*- fscan==.*/d' $ENVFILE
sed -i '/.*- daiquiri==.*/d' $ENVFILE

########################################

ENVFILE=scripts/requirements/bliss.yml
ENV=tomodemo-bliss-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE
sed -i '/.*- lima-.*=.*/d' $ENVFILE
sed -i '/.*- lima-server==.*/d' $ENVFILE
sed -i '/.*- bliss-tomo==.*/d' $ENVFILE
sed -i '/.*- bliss==.*/d' $ENVFILE
sed -i '/.*- blisswriter==.*/d' $ENVFILE
sed -i '/.*- blissdemo==.*/d' $ENVFILE
# sed -i '/.*- nxtomomill==.*/d' $ENVFILE
# sed -i '/.*- tomoscan==.*/d' $ENVFILE
sed -i '/.*- fscan==.*/d' $ENVFILE

########################################

ENVFILE=scripts/requirements/tomovis+sidecar+nabu.yml
ENV=tomodemo-tomovis-env

echo  $ENV
conda env export --name $ENV --file $ENVFILE
sed -i '/name:.*/d' $ENVFILE
sed -i '/prefix:.*/d' $ENVFILE
sed -i '/.*- bliss-tomo==.*/d' $ENVFILE
sed -i '/.*- tomovis==.*/d' $ENVFILE
sed -i '/.*- sidecar==.*/d' $ENVFILE

########################################
