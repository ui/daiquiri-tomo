#
# Run a command inside a conda env
#
# "conda run" exists as conda command but spawn another process with subprocess
# which make supervisor a bit confuse.
#

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh


# To have a separator inside supervisor logs
T=$(date +%R)
echo "$T mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"

conda activate $1

exec ${@:2}
