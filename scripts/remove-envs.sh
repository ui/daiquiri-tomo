set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

conda env remove -y -n tomodemo-tools-env
conda env remove -y -n tomodemo-bliss-env
conda env remove -y -n tomodemo-daiquiri-env
conda env remove -y -n tomodemo-ui-env
conda env remove -y -n tomodemo-daiquiri-stream-env
conda env remove -y -n tomodemo-tomovis-env
conda env remove -y -n tomodemo-lima-env
