set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

recreate_and_activate_env() {
  conda env remove -n $1
  mamba create --yes -n $1
  conda activate $1
  conda config --env --set channel_priority false
  conda config --env --add channels conda-forge
  conda config --env --remove channels defaults
  # conda config --env --append channels esrf-bcu
  # conda config --env --append channels tango-controls
}

# Try to setup working envs

# Generate env in order to freeze them
#
# conda env export > my_environment.yml

set +x
echo "========================================="
echo "              DAIQUIRI ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-daiquiri-env
# BLISS also have to be installed there

# cd bliss.git
# make bl_env NAME=tomodemo-daiquiri-env
# make dev_env NAME=tomodemo-daiquiri-env
# cd ..

mamba install --file daiquiri.git/requirements-conda.txt celery
pip install -r daiquiri.git/requirements-test.txt -e ./daiquiri.git

# pip install -e bliss.git/blissdata
# pip install -e bliss.git
pip install -e ./fscan.git
pip install -e ./bliss-tomo.git
pip install --no-deps -e ./id00.git

conda deactivate

set +x
echo "========================================="
echo "                   DONE"
echo "========================================="
set -x
