set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

set +x
echo "========================================="
echo "              TOOLS ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-tools-env --file ./scripts/requirements/tools.yml

set +x
echo "========================================="
echo "              BLISS ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-bliss-env --file ./scripts/requirements/bliss.yml

conda activate tomodemo-bliss-env
pip install -e id00.git/ --no-deps
pip install -e bliss.git/blissdata --no-deps
pip install -e bliss.git --no-deps
pip install -e fscan.git --no-deps
pip install -e bliss-tomo.git --no-deps
conda deactivate

set +x
echo "========================================="
echo "              DAIQUIRI ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-daiquiri-env --file ./scripts/requirements/daiquiri.yml

conda activate tomodemo-daiquiri-env
mamba install celery
pip install -e id00.git/ --no-deps
pip install -e bliss.git/blissdata --no-deps
pip install -e bliss.git --no-deps
pip install -e fscan.git --no-deps
pip install -e bliss-tomo.git --no-deps
pip install -e daiquiri.git/ --no-deps
conda deactivate

set +x
echo "========================================="
echo "                 UI ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-ui-env --file ./scripts/requirements/ui.yml

conda activate tomodemo-ui-env
#npm install -g pnpm
pushd daiquiri-ui.git
pnpm install
mkdir -p build
popd
conda deactivate

set +x
echo "========================================="
echo "              TOMOVIS ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-tomovis-env --file ./scripts/requirements/tomovis+sidecar+nabu.yml

conda activate tomodemo-tomovis-env
pip install -e sidecar.git/ --no-deps
pip install -e bliss-tomo.git/ --no-deps
pip install -e tomovis.git/ --no-deps
conda deactivate

set +x
echo "========================================="
echo "              LIMA ENV"
echo "========================================="
set -x

mamba env create -n tomodemo-lima-env --file ./scripts/requirements/lima.yml
conda activate tomodemo-lima-env
pip install -e bliss-tomo.git/ --no-deps
pip install -e bliss.git/blissdata --no-deps
pip install -e bliss.git/ --no-deps
conda deactivate
