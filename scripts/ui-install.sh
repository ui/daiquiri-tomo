set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

conda activate tomodemo-ui-env

cd daiquiri-ui.git
pnpm install --frozen-lockfile

set +x
cd ..
conda deactivate
