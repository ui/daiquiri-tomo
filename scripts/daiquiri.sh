export TANGO_HOST=localhost:10000
export BEACON_HOST=localhost:10001

PWD=$(pwd)
CMD="./scripts/conda-run.sh tomodemo-daiquiri-env daiquiri-server --config=./daiquiri_private/daiquiri.yml $*"
echo ${CMD}
${CMD}