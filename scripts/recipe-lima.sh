set -x
set -e

CONDA_HOME=$(conda info --base)
. $CONDA_HOME/etc/profile.d/conda.sh

recreate_and_activate_env() {
  conda env remove -n $1
  mamba create --yes -n $1
  conda activate $1
  conda config --env --set channel_priority false
  conda config --env --add channels conda-forge
  conda config --env --remove channels defaults
  conda config --env --append channels esrf-bcu
  # conda config --env --append channels tango-controls
}

# Try to setup working envs

# Generate env in order to freeze them
#
# conda env export > my_environment.yml

set +x
echo "========================================="
echo "              LIMA ENV"
echo "========================================="
set -x

recreate_and_activate_env tomodemo-lima-env
mamba install --yes --file scripts/requirements/lima.txt

# Subset of bliss dependencies
pip install tabulate louie jedi ruamel.yaml msgpack_numpy tblib prompt-toolkit

pip install -e bliss.git/blissdata
pip install -e bliss-tomo.git/ --no-deps
pip install -e bliss.git/ --no-deps

conda deactivate

set +x
echo "========================================="
echo "                   DONE"
echo "========================================="
set -x
