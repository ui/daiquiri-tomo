"""
BE CAREFUL THIS SCRIPT IS ALSO EXECUTED BY DAIQUIRI AT STARTUP

DONT EXPECT IT TO RUN ONLY WITH THE BLISS SESSION
"""

from bliss.config import static
from bliss.shell.standard import umv
from tomo.scan.presets.dark_shutter import DarkShutterPreset
from tomo.scan.presets.fast_shutter import FastShutterPreset
from tomo.scan.presets.reference_motor import ReferenceMotorPreset

#######################################

config = static.get_config()
energy = config.get("energy")
fe_current = config.get("fe_current")
sx = config.get("sx")
sy = config.get("sy")
sz = config.get("sz")
beam_shutter = config.get("beam_shutter")

#######################################

energy.position = 13
fe_current.position = 4
if sx.position == 0:
    sx.acceleration = 0.1

dark_preset = DarkShutterPreset(beam_shutter)
fast_shutter = FastShutterPreset(beam_shutter)


def active_tomo1():
    """Set tomo1 as active tomo config"""
    tomo1_counters = config.get("tomo1_counters")
    tomo1_config = config.get("tomo1_config")

    tomo1_counters.set_active()
    tomo1_config.set_active()
    tomo1_config.reference.set_out_of_beam_position(tomo1_config.y_axis, 20)


def active_tomo2():
    """Set tomo1 as active tomo config"""
    tomo2_counters = config.get("tomo2_counters")
    tomo2_config = config.get("tomo2_config")

    flat2_motor = ReferenceMotorPreset(tomo2_config.reference)
    tomo2_config.get_runner("dark").add_scan_preset(dark_preset)
    tomo2_config.get_runner("flat").add_scan_preset(flat2_motor)
    tomo2_config.get_runner("flat").add_scan_preset(fast_shutter)
    tomo2_config.get_runner("return_ref").add_scan_preset(fast_shutter)
    tomo2_config.get_runner("tiling").add_scan_preset(fast_shutter)
    tomo2_config.get_runner("count").add_scan_preset(fast_shutter)
    tomo2_config.get_runner("CONTINUOUS").add_scan_preset(fast_shutter)
    tomo2_config.get_runner("STEP").add_scan_preset(fast_shutter)

    tomo2_counters.set_active()
    tomo2_config.set_active()
    tomo2_config.reference.set_out_of_beam_position(tomo2_config.y_axis, 200)


def active_tomo3():
    """Set tomo3 as active tomo config"""
    hr3counters = config.get("hr3counters")
    tomo3_config = config.get("tomo3_config")

    flat3_motor = ReferenceMotorPreset(tomo3_config.reference)
    tomo3_config.get_runner("dark").add_scan_preset(dark_preset)
    tomo3_config.get_runner("flat").add_scan_preset(flat3_motor)
    tomo3_config.get_runner("flat").add_scan_preset(fast_shutter)
    tomo3_config.get_runner("return_ref").add_scan_preset(fast_shutter)
    tomo3_config.get_runner("tiling").add_scan_preset(fast_shutter)
    tomo3_config.get_runner("count").add_scan_preset(fast_shutter)
    tomo3_config.get_runner("CONTINUOUS").add_scan_preset(fast_shutter)
    tomo3_config.get_runner("STEP").add_scan_preset(fast_shutter)

    hr3counters.set_active()
    tomo_config = tomo3_config
    if tomo_config.sample_stage.source_distance is None:
        # tomo3_config.x_axis.position = 79.908700
        tomo_config.sample_stage.source_distance = 1.0
        sx.position = 9.0
        tomo_config.x_axis.limits = [-2.0, 80.0]
    tomodet = tomo_config.detectors.active_detector
    if tomodet is not None and tomodet.source_distance is None:
        tomodet.tomo_detectors.detector_axis.position = -71.000000
        tomodet.source_distance = 10.0
    tomo_config.set_active()
    tomo3_config.reference.set_out_of_beam_position(tomo3_config.y_axis, 1500)
    if tomo3_config.detectors.active_detector is None:
        sy.custom_set_measured_noise(0.002)
        sz.custom_set_measured_noise(0.002)


def active_id16a():
    """Set id16a as active tomo config"""
    id16a_counters = config.get("id16a_counters")
    id16a_config = config.get("id16a_config")

    id16a_motor = ReferenceMotorPreset(id16a_config.reference)
    id16a_config.get_runner("dark").add_scan_preset(dark_preset)
    id16a_config.get_runner("flat").add_scan_preset(id16a_motor)
    id16a_config.get_runner("flat").add_scan_preset(fast_shutter)
    id16a_config.get_runner("return_ref").add_scan_preset(fast_shutter)
    id16a_config.get_runner("tiling").add_scan_preset(fast_shutter)
    id16a_config.get_runner("count").add_scan_preset(fast_shutter)
    id16a_config.get_runner("CONTINUOUS").add_scan_preset(fast_shutter)
    id16a_config.get_runner("STEP").add_scan_preset(fast_shutter)

    id16a_counters.set_active()
    tomo_config = id16a_config
    if tomo_config.sample_stage.source_distance is None:
        # tomo3_config.x_axis.position = 79.908700
        tomo_config.sample_stage.source_distance = 1.0
        sx.position = 0.0
        tomo_config.x_axis.limits = [-1.0, 10.0]
    tomodet = tomo_config.detectors.active_detector
    if tomodet is not None and tomodet.source_distance is None:
        tomodet.tomo_detectors.detector_axis.position = -71.000000
        tomodet.source_distance = 10.0
    tomo_config.set_active()
    id16a_config.flat_motion.set_out_of_beam_position(id16a_config.y_axis, 1500)

    sample_stage = id16a_config.sample_stage
    sample_stage.focal_position = -1, 0, 0

    beam_tracker = id16a_config.beam_tracker
    beam_tracker.sy_angle = -1.5  # about -14mm for dx=1
    beam_tracker.sz_angle = 1.55  # about 48mm for dx=1

    somega = config.get("somega")
    somega.limits = [-10, 190]


def active_id16b():
    """Set id16b as active tomo config

    The common holotomo pixel size used is 50nm
    """
    id16b_counters = config.get("id16b_counters")
    id16b_config = config.get("id16b_config")

    id16b_motor = ReferenceMotorPreset(id16b_config.reference)
    id16b_config.get_runner("dark").add_scan_preset(dark_preset)
    id16b_config.get_runner("flat").add_scan_preset(id16b_motor)
    id16b_config.get_runner("flat").add_scan_preset(fast_shutter)
    id16b_config.get_runner("return_ref").add_scan_preset(fast_shutter)
    id16b_config.get_runner("tiling").add_scan_preset(fast_shutter)
    id16b_config.get_runner("count").add_scan_preset(fast_shutter)
    id16b_config.get_runner("CONTINUOUS").add_scan_preset(fast_shutter)
    id16b_config.get_runner("STEP").add_scan_preset(fast_shutter)

    id16b_counters.set_active()
    tomo_config = id16b_config
    if tomo_config.sample_stage.source_distance is None:
        tomo_config.sample_stage.source_distance = 1.0
        sx.position = 0.0
        tomo_config.x_axis.limits = [-3.0, 10.0]
    tomodet = tomo_config.detectors.active_detector
    if tomodet is not None and tomodet.source_distance is None:
        tomodet.tomo_detectors.detector_axis.position = 705
        tomodet.source_distance = 704.312
    tomo_config.set_active()
    id16b_config.flat_motion.set_out_of_beam_position(id16b_config.y_axis, 1500)

    sample_stage = id16b_config.sample_stage
    sample_stage.focal_position = 0.688, 0, 0

    beam_tracker = id16b_config.beam_tracker
    beam_tracker.sy_angle = -1.5  # about -14mm for dx=1
    beam_tracker.sz_angle = 1.55  # about 48mm for dx=1

    spush = config.get("spush")
    spush.controller._set_home_pos(spush, 90.0)


# active_tomo3()
active_id16b()
